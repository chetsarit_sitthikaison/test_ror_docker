FROM ubuntu:14.04
MAINTAINER chetsarit

RUN apt-get update && apt-get install -y curl
RUN \curl -sSL https://get.rvm.io | bash -s stable --ruby=1.9.3

ONBUILD ADD . /app

WORKDIR /app

RUN bundle install
RUN bundle install

EXPOSE 3000

CMD rails s